from core.compute_unit import *
from core.config import success_condition
from core.questions import IsLogin, AboutSQL, ParamIsNecessary, ParamHasGroup, ParamType

success_c = verify_success(success_condition)
sol_order = [ParamType, AboutSQL, IsLogin, ParamIsNecessary, ParamHasGroup]

def build_solutions():
    with IsLogin.register() as s:
        s.cal_flow = Source(url='url').my_filter(success_c).update(remove_headers).update(resend).summary(
            my_all(my_not(success_c)))
        s.res_builder = lambda res: json.dumps(res)

    with AboutSQL.register() as s:
        s.cal_flow = Source(url='url').my_filter(success_c).gate(make_order(lambda ask: -ask.date)).my_filter(
            diff_param).update(resend).listen(remember_sql)
        s.res_builder = lambda res: json.dumps(list(res))

    with ParamIsNecessary.register() as s:
        s.condition = lambda model: model.only_selected()
        s.cal_flow = Source(url='url').my_filter(success_c).expand(remove_one_required).my_filter(
            no_checked_param).update(resend).my_filter(success_c).expand(update_required)
        s.res_builder = lambda res: json.dumps(list(res))

    with ParamHasGroup.register() as s:
        s.condition = lambda model: model.only_selected()
        s.cal_flow = Source(url='url').my_filter(success_c).model_update(remove_all_group).update(
            remove_not_required).update(resend).summary(my_all(my_not(success_c))).IF(
            success=Source(url='url').expand(one_not_required)).update(resend).summary(summary_group(success_c))
        s.res_builder = lambda res: json.dumps(list(res))

    with ParamType.register() as s:
        s.cal_flow = Source(url='url').my_filter(success_c).gate(get_all_routes_and_values).update(summarize_param_type)
        s.res_builder = lambda res: json.dumps(list(res))

