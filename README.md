# auto-Api

#### 项目介绍
全自动化API文档，支持任意语言和框架。


##### 优势：
1. 搭建好后，自动化构建文档，不用再在文档维护上花费更多心力。API服务器软件更新，文档同步更新只用简单点击按钮即可
2. 高度可定制空间，对特殊的API的开发规格适配比较容易(阅读下wiki)
3. 使用便捷，安装方便(个鬼，具体的安装方法我自己没在其他机子试过，如果有问题请联系我)。
4. 作者好联系，有麻烦绝对会出来解决
#### 特性
* 支持任意语言与框架的服务端程序，构建API文档没有任何侵入代码。
* 一键式快速构建API文档，一键式更新文档。部署好后不用写一行文档。
* 高度可定制空间，为不同的API规范留下充足的支持空间
* 一定程度上比人工文档更加精确和详细，开发人员可以快速理解参数的用途
* 支持markdown注释，保留人工解释空间。文档内容更加清晰
* 细化模型高度解耦和形象化，为第三方程序或脚本留下便捷的实现空间
* 支持 gitlab OAUTH2 授权认证，安全防止文档泄露
* 支持数据库文档一键生成
* 使用vue.js + webpack 开发前端界面，响应迅速，无缝体验
* 支持快速体验，实际使用请求，加深对API的理解

#### 使用界面
**2019-1-12更新界面，在之后可能有所变动**

1. 主页
![主页](https://images.gitee.com/uploads/images/2019/0112/163120_39aee46f_602344.png "屏幕截图.png")

2. 文档

![文档](https://images.gitee.com/uploads/images/2019/0112/163429_2b4daed5_602344.png "屏幕截图.png")

![返回值](https://images.gitee.com/uploads/images/2019/0112/163513_5e2e5ae2_602344.png "屏幕截图.png")

![相关信息](https://images.gitee.com/uploads/images/2019/0112/163536_7360a0da_602344.png "屏幕截图.png")

![注释](https://images.gitee.com/uploads/images/2019/0112/163747_dda88679_602344.png "屏幕截图.png")

![相关数据库](https://images.gitee.com/uploads/images/2019/0112/164823_e089e810_602344.png "屏幕截图.png")

3. 支持一键数据库文档生成


![数据库](https://images.gitee.com/uploads/images/2019/0112/164116_1e352a50_602344.png "屏幕截图.png")

#### 软件架构
绿色的是主体程序，黄色的是可选的部分(涉及一些API自动理解的计算，刚开始可以不用管)
![项目架构](https://images.gitee.com/uploads/images/2018/1204/164006_d93f4378_602344.jpeg "项目架构.jpg")



#### 安装教程
看wiki，不会安装可以联系我
[入门安装和配置详细指南](https://gitee.com/easyGroup/auto-Api/wikis/%E5%85%A5%E9%97%A8%E5%AE%89%E8%A3%85%E5%92%8C%E9%85%8D%E7%BD%AE%E8%AF%A6%E7%BB%86%E6%8C%87%E5%8D%97?sort_id=1120662)

#### 额外的进阶内容
1. 一些细节可以在这个项目的wiki上看
2. 数据库代理目前只测试过PostgreSql。要求通讯中不能被加密，这样就能记录某个请求产生的影响

#### 待完成内容
看issue吧，有需要的功能也可以开个新的issue

#### 参与贡献
1. 提Issue 把使用中遇到的问题告诉我
2. 点个star 让我开心下
3. 提PR，非常感谢。


#### 联系我
有任何使用问题，都可以联系我
QQ群：604437106 <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=c3125a5cc324e3636d63943b752481e67b1c851382ab53135d0bdf892c2b7dc8"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="自动化文档" title="自动化文档"></a>