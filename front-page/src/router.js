import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import DocContent from './views/DocContent';
import SearchResult from './views/SearchResult';
import SqlContent from "./views/SqlContent";

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/doc/',
      name: 'doc',
      component: DocContent,
    },
    {
      path: '/search/:queryContent',
      name: 'search',
      component: SearchResult,
    },
    {
      path: '/sql/:table',
      name: 'sql_table',
      component: SqlContent,
    },
  ],
});
