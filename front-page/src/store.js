import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data_iterable: 0,
    sql_table_names: [],
  },
  mutations: {

  },
  actions: {

  },
});
