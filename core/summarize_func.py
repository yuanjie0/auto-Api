import json


def s_Enum(source, new_one):
    source = json.loads(source)
    new_one = json.loads(new_one)
    if new_one[0] not in source:
        source.append(new_one[0])
    return json.dumps(source, ensure_ascii=False)


def s_Range(source, new_one, value_type):
    new_one = json.loads(new_one)[0]
    source = json.loads(source)
    if not len(source):
        return json.dumps([[new_one, new_one]])
    for rg in range(len(source)):
        if source[rg][1] > new_one:  # 就在这里建立连接
            if value_type == str(float):
                source.insert(rg, [new_one, new_one])
            elif value_type == str(int):  # 判断是否连接
                if source[rg][1] + 1 == new_one:
                    source[rg][1] = new_one
                if rg + 1 < len(source):
                    if source[rg + 1][0] - 1 == new_one:
                        source[rg + 1][0] = new_one
                    if source[rg + 1][0] <= source[rg][1]:
                        # 合并
                        source[rg][1] = source[rg + 1][1]
                        del source[rg + 1]

        if source[rg][0] >= new_one >= source[rg][1]:
            return json.dumps(source)


