class WarningLv:
    ERROR = 1
    WARNING = 2
    INFO = 3
    SQL_VAL_CHECK_ONE_TIME_WARNING = '{route} 只有一种请求参数，对应的数据库信息可能不准确'


class ApiType:
    SELECT = 'SELECT'
    UPDATE = 'UPDATE'
    INSERT = 'INSERT'


class RangeType:
    SP_LEN = 'sp_len'
    VAR_LEN = 'var_len'
    CONFLICT = 'conflict'
    ENUM = 'enum'
    SQL_TABLE = 'SQL_TABLE'
    UNBOUND = 'unbound'
    RANGE = 'range'
    HISTORY = 'history'


class GroupType:
    SELECT_ONE = 'select_one'  # n 选一
    BINDING = 'binding'  # 必须同时出现


class IntCheck:
    @classmethod
    def check(cls, value):
        try:
            int(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'int'


class FloatCheck:
    @classmethod
    def check(cls, value):
        try:
            float(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'float'


class BoolCheck:
    @classmethod
    def check(cls, value):
        return isinstance(value, bool) or value in ['True', 'False']

    def __str__(self):
        return 'bool'


class StrCheck:
    @classmethod
    def check(cls, value):
        try:
            str(value)
        except Exception:
            return False
        return True

    def __str__(self):
        return 'str'


BaseTypeList = [BoolCheck, IntCheck, FloatCheck, StrCheck]
