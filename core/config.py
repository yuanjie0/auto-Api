import configparser
from configparser import NoOptionError

cf = configparser.ConfigParser()
cf.read('proxy.conf', encoding='utf-8')
LISTEN_PORT = cf.getint('server', 'LISTEN_PORT')
GOAL_PORT = cf.getint('server', 'GOAL_PORT')

OUTER_URL = cf.get('server', 'OUTER_URL')[1:-1]
MONGODB_DB = cf.get('mongodb', 'DB')[1:-1]  # 去除引号
MONGODB_HOST = cf.get('mongodb', 'HOST')[1:-1]
GOAL_URL = cf.get('server', 'GOAL_URL')[1:-1]

OAUTH2_key = cf.get('secret', 'OAUTH2_key')[1:-1]
OAUTH2_secret = cf.get('secret', 'OAUTH2_secret')[1:-1]
OAUTH2_URL = cf.get('secret', 'OAUTH2_URL')[1:-1]
OAUTH2_OPEN = cf.get('secret', 'OAUTH2_OPEN')[1:-1] == 'True'
SECRET_KEY = cf.get('secret', 'SECRET_KEY')[1:-1]
WEB_CONSOLE_ADDRESS = cf.get('web_console', 'ADDRESS')[1:-1]
WEB_CONSOLE_PORT = cf.getint('web_console', 'PORT')


def get_conditon():
    condition = {}
    i = 0
    while True:
        i += 1
        try:
            cond = cf.get('valid_req', 'cond{}'.format(i))
            cond = tuple(cond[1:-1].split('.'))
            valid = cf.get('valid_req', 'valid{}'.format(i))[1:-1]
            condition[cond] = lambda x: eval(valid)
        except NoOptionError:
            break
    return condition


success_condition = get_conditon()
