from flask import request

from api.libs.RedPrint import RedPrint
from core.documents import AskProxySetting

api = RedPrint('proxy')


@api.route('/state')
def state():
    state = AskProxySetting.get_state()
    result = [{'content': '记录请求', 'goal_state': 'listenAsk'},
              {'content': '代理去重--请求参数', 'goal_state': 'use_data'},
              {'content': '代理去重--返回结果', 'goal_state': 'use_result'},
              {'content': '代理去重--请求头', 'goal_state': 'use_header'}]
    for i, goal_state in enumerate(['listenAsk', 'use_data', 'use_result', 'use_header']):
        result[i]['status'] = '开启' if state[goal_state] else '关闭'
    return result


@api.route('/change_status')
def change_status():
    goal_state = request.values['goal_state']
    from core.documents import AskProxySetting
    state = AskProxySetting.get_state()
    state[goal_state] = not state[goal_state]
    state.save()
    return '开启' if state[goal_state] else '关闭'
