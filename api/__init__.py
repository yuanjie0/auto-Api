from flask import Blueprint

from api import doc, sql, common, model, proxy,ask


def build_buleprint():
    bp = Blueprint('api', __name__, url_prefix='/api')
    doc.api.register(bp)
    sql.api.register(bp)
    common.api.register(bp)
    model.api.register(bp)
    proxy.api.register(bp)
    ask.api.register(bp)
    return bp
