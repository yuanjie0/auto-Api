import functools
import json

from core.decorate import need_login


def return_json(f):
    @functools.wraps(f)
    def decorate(*args, **kwargs):
        result = f(*args, **kwargs)
        return json.dumps(result,ensure_ascii=False)

    return decorate


class RedPrint:
    def __init__(self, name):
        self.name = name
        self.mount = []

    def route(self, rule, **options):
        def decorate(f):
            self.mount.append((need_login(return_json(f)), rule, options))
        return decorate

    def register(self, bp):
        for f, rule, options in self.mount:
            bp.route('/{}{}'.format(self.name, rule), **options)(f)
