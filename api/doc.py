from flask import request

from api.libs.RedPrint import RedPrint

api = RedPrint('doc')


@api.route('/param')
def doc_param():
    url = request.values['url']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    root = model.args.get_data_content()
    if 'children' not in root:
        root['children'] = []
    return root['children']


@api.route('/result')
def doc_result():
    url = request.values['url']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    root = model.result.get_data_content()
    if 'children' not in root:
        root['children'] = []
    return root['children']


@api.route('/addition_info')
def addition_info():
    url = request.values['url']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    others = [["{}需要登陆".format('' if model.need_login else '不'),
               model.method,
               "共记录了{}条请求".format(model.get_asks_count())],
              model.get_asks_count()]
    return others


@api.route('/comment')
def doc_comment():
    url = request.values['url']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    return model.comment


@api.route('/sql')
def doc_sql():
    url = request.values['url']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    res = []
    for table in model.aboutSql:
        for operation in model.aboutSql[table]:
            res.append({'name': table, 'operationType': operation})
    return res


@api.route('/rawSql')
def rawSql():
    url = request.values['url']
    name = request.values['name']
    operationType = request.values['operationType']
    from core.documents import APIModel
    model = APIModel.objects(relate_url=url).first()
    result = []
    for raw in model.aboutSql[name][operationType]:
        result.append(raw[1])
    return result


@api.route('/questions')
def getQuestions():
    url = request.values['url']
    from core.documents import APIModel,QuestionState
    from core.questions import registered_questions
    model = APIModel.objects(relate_url=url).first()
    res = []
    for que in registered_questions:
        question = registered_questions[que].get_about_question(model)
        state = QuestionState.objects(que_type = que).first()
        res.append(
            {'name': registered_questions[que].get_comment(),
             'status': question.solved,
             'content': question.result,
             'que_key': question.key,
             'available': registered_questions[que].can_solve(),
             })
    return res


@api.route('/remove_ans')
def remove_ans_api():
    from core.documents import Question
    key = request.values['que_key']
    question = Question.objects(key=key).first()
    question.get_question().re_init_model(question.about)
    question.delete()
    return True


@api.route('/upload_ans', methods=['POST'])
def upload_ans():
    from core.documents import Question
    key = request.form['query_key']
    content = request.form['content']
    question = Question.get_question_by_key(key=key)
    question.result = content
    question.solved = True
    question.save()
    return True


@api.route('/auto_solve_one')
def auto_solve_one():
    from core.documents import Question
    from core.questions import QueBase
    que_key = request.values['queryKey']
    que = Question.get_question_by_key(que_key)
    QueBase.solve(que)
    return True


@api.route('/set_ignore')
def set_ignore():
    url = request.values['url']
    from core.documents import APIModel, IgnoreApi
    model = APIModel.objects(relate_url=url).first()
    if model:
        if 'is_ignore' in request.values:
            is_ignore = request.values['is_ignore']
            if is_ignore != 'false':
                IgnoreApi(url=model.url).save()
            else:
                IgnoreApi.objects(url=model.url).delete()
            return is_ignore == 'false'
        else:
            if IgnoreApi.objects(url=model.url).first():
                return True
            else:
                return False
    return False


@api.route('/clean_ask')
def clean_ask():
    from core.documents import Ask, APIModel
    url = request.values['url']
    count = int(request.values['count'])
    model = APIModel.objects(relate_url=url).first()
    Ask.objects(url=model.url).order_by('-date')[count:].delete()
    return True
