// Copyright 2017 wgliang. All rights reserved.
// Use of this source code is governed by Apache
// license that can be found in the LICENSE file.

// Package parser provides filtering rules if you need.
package parser

import (
	"../config"
	"fmt"
	"gopkg.in/mgo.v2"
	"time"
)

type SqlAsk struct {
	RawComment string
	Time       int64
}

var conn *mgo.Database

// Callback function from proxy to postgresql for rewrite
// request or sql.
type Callback func(get []byte) bool

// Extracte sql statement from string
func Extracte(str []byte) string {
	return string(str)[5:]
}



func Filter(str []byte) bool {
	//*
	if !config.OnRemember(){
		return true
	}
	sql := Extracte(str)
	conn = config.ConnectToDB()
	//*
	c := conn.C("SQLAsk")
	ask := SqlAsk{}
	ask.RawComment = sql
	ask.Time = time.Now().Unix()
	c.Insert(ask)
	return true

}

func Return(str []byte) bool {
	fmt.Println(string(str))
	return true
}
